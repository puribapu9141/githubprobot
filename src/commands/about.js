module.exports = bot => {
  bot.onText(/\/about/, message => {
    const fromId = message.from.id;
    const response = `
Developer - Para-Commander.

Follow me on Telegram - https://telegram.com/Para_Commander_Chat
Follow me on GitHub - https://github.com/Para-Commander.

You can report issues directly to me via Telegram and I'll respond as soon as possible, thanks.
`;

    return bot.sendMessage(fromId, response);
  });
};
